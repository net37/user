/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.user.nettigatujuhuser.dto;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author Moh Machfudh
 */
@Data
@Entity
@Table(name = "net_agent")
public class Agent {
    
    @Id
    private String id;
    private String id_user;
    private String panggil;
    private String nama;
    private String jeniskel;
    private String tgllahir;
    private String nohp;
    private String email;
    private String alamat;
    private String kodepos;
    private String noktp;
    private String nonpwp;
    private String norekening;
    private String photodiri;
    private String photoktp;
    private String noupline;
    private String noanggota;
    private String inserid;
    private String insertdate;
    private String editid;
    private String editdate;
    private boolean active; 
    private String status;
    
}

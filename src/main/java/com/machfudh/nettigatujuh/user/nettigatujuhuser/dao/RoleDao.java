/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.user.nettigatujuhuser.dao;

import com.machfudh.nettigatujuh.user.nettigatujuhuser.entity.Role;
import java.io.Serializable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Moh Machfudh
 */
@Repository
public interface RoleDao extends PagingAndSortingRepository<Role, String>{
    
//    @Query("select id from net_role where name =: name")
    public Role findByName(String name);
    
}

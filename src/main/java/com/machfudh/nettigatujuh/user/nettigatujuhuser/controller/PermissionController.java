/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.user.nettigatujuhuser.controller;

import com.machfudh.nettigatujuh.user.nettigatujuhuser.dao.PermissionDao;
import com.machfudh.nettigatujuh.user.nettigatujuhuser.entity.Permission;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Moh Machfudh
 */
@Controller
public class PermissionController {

    @Autowired
    private PermissionDao permissionDao;

    @PreAuthorize("hasAuthority('USER_VIEW')")
    @GetMapping("/api/permission")
    @ResponseBody
    public Page<Permission> dataPermission(Pageable page) {
        return permissionDao.findAll(page);
    }

    @PreAuthorize("hasAuthority('USER_EDIT')")
    @PostMapping("/api/permission")
    @ResponseStatus(HttpStatus.CREATED)
    public void savePermission(@RequestBody @Valid Permission p) {
        permissionDao.save(p);
    }

}

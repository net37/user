/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.user.nettigatujuhuser.dao;

import com.machfudh.nettigatujuh.user.nettigatujuhuser.dto.Agent;
import java.io.Serializable;
import javax.swing.Spring;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Moh Machfudh
 */
@Repository
public interface AgentDao extends PagingAndSortingRepository<Agent, String>{
    
    public Agent findByEmailAndNoanggota(String email, String noanggota);
}

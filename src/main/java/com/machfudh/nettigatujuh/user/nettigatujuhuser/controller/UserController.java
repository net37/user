/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.user.nettigatujuhuser.controller;

import com.machfudh.nettigatujuh.user.nettigatujuhuser.dao.AgentDao;
import com.machfudh.nettigatujuh.user.nettigatujuhuser.dao.RoleDao;
import com.machfudh.nettigatujuh.user.nettigatujuhuser.dao.UserDao;
import com.machfudh.nettigatujuh.user.nettigatujuhuser.dao.UserRoleDao;
import com.machfudh.nettigatujuh.user.nettigatujuhuser.dto.Agent;
import com.machfudh.nettigatujuh.user.nettigatujuhuser.dto.Registrasi;
import com.machfudh.nettigatujuh.user.nettigatujuhuser.dto.RespRegsiter;
import com.machfudh.nettigatujuh.user.nettigatujuhuser.entity.Role;
import com.machfudh.nettigatujuh.user.nettigatujuhuser.entity.User;
import com.machfudh.nettigatujuh.user.nettigatujuhuser.entity.UserRole;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Moh Machfudh
 */
@Controller
public class UserController {

    @Autowired
    private UserDao userDao;
    
    @Autowired
    private AgentDao agentDao;
    
    @Autowired
    private UserRoleDao userRoleDao;
    
    @Autowired
    private RoleDao roleDao;
    
    private Date tglsnow;

    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @PreAuthorize("hasAuthority('USER_VIEW')")
    @GetMapping("/api/user")
    @ResponseBody
    public Page<User> dataUser(Pageable page) {
        return userDao.findAll(page);
    }

    @PreAuthorize("hasAuthority('USER_ADD')")
    @PostMapping("/api/user")
    @ResponseStatus(HttpStatus.CREATED)
    public void saveUser(@RequestBody @Valid User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userDao.save(user);
    }

    @PreAuthorize("hasAuthority('USER_EDIT')")
    @PutMapping("api/user/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void editUser(@PathVariable(name = "id") String id, @RequestBody @Valid User user) {
        User newUser = userDao.findOne(id);

        if (newUser == null) {
            return;
        }
        BeanUtils.copyProperties(user, newUser);
        newUser.setId(id);
        newUser.setPassword(passwordEncoder.encode(newUser.getPassword()));
        userDao.save(newUser);
    }
    
    @PreAuthorize("hasAuthority('USER_ADD')")
    @PostMapping("/api/signup")
    @ResponseBody
    public RespRegsiter registerUser(@RequestBody @Valid Registrasi registrasi){
        tglsnow = new Date();
        
        RespRegsiter respRegsiter = new RespRegsiter(); 
        
        //cek email pada data user
        User user = userDao.findByUsername(registrasi.getUsername());
        if( user == null ){
            System.out.println("=========================   nulll:");
            Agent agent = agentDao.findByEmailAndNoanggota(registrasi.getUsername(), registrasi.getNoanggota());
            if(  agent != null) {
                //insert user
                User newUser = new User();
                newUser.setUsername(registrasi.getUsername());
                newUser.setPassword(passwordEncoder.encode(registrasi.getPassword()));
                newUser.setEnabled(true);
                newUser.setUserdate(Long.toString(tglsnow.getTime()));
                userDao.save(newUser);

                System.out.println("simpan Data ========================= ");
                User userI = userDao.findByUsername(registrasi.getUsername());
                if( userI != null ){
                    // insert role -> agent
//                    Role role = roleDao.findByName("Agent");
//                    if ( role != null ){
                        UserRole ur = new UserRole();
                        ur.setId_user(userI.getId());
                        ur.setId_role("8da9aef7-04c6-4831-85cf-cabe45ddf717");
                        
                        userRoleDao.save(ur);
                        System.out.println(" user role ================================= success" );
                        
                        agent.setId_user(userI.getId());
                        agentDao.save(agent);
                        
                        System.out.println(" user Agent ================================= success" );
//                    }
                } 
            }   
            respRegsiter.setStatus("200");
            respRegsiter.setMessage("Registrasi User Berhasil");
            return respRegsiter; 
        }
        
        System.out.println("=========================   dududul :");
        respRegsiter.setStatus("400");
        respRegsiter.setMessage("Registrasi User Gatooooottt");
        return respRegsiter;
        
    }
    
    
    
}

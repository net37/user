/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.user.nettigatujuhuser.dao;

import com.machfudh.nettigatujuh.user.nettigatujuhuser.entity.User;
import org.jboss.logging.Param;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Moh Machfudh
 */
@Repository
public interface UserDao extends PagingAndSortingRepository<User, String>{
    
//   @Query("select u.* from net_user u where u.username =:emai") 
   public User findByUsername(String email);   
    
}

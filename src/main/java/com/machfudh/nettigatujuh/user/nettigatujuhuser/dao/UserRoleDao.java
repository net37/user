/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.user.nettigatujuhuser.dao;

import com.machfudh.nettigatujuh.user.nettigatujuhuser.entity.UserRole;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Moh Machfudh
 */
public interface UserRoleDao  extends PagingAndSortingRepository<UserRole, String>{
    
}

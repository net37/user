/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.user.nettigatujuhuser.controller;

import com.machfudh.nettigatujuh.user.nettigatujuhuser.dao.RoleDao;
import com.machfudh.nettigatujuh.user.nettigatujuhuser.entity.Role;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Moh Machfudh
 */
@Controller
public class RoleController {
    
    @Autowired
    private RoleDao roleDao;
    
    @PreAuthorize("hasAuthority('USER_VIEW')")
    @GetMapping("/api/role")
    @ResponseBody
    public Page<Role> dataRole(Pageable page) {
        return roleDao.findAll(page);
    }

    @PreAuthorize("hasAuthority('USER_EDIT')")
    @PostMapping("/api/role")
    @ResponseStatus(HttpStatus.CREATED)
    public void saveRole(@RequestBody @Valid Role role) {
        roleDao.save(role);
    }
    
    
    
}

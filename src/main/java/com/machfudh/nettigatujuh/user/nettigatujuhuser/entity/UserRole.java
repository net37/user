/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.user.nettigatujuhuser.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Moh Machfudh
 */

@Data
@Entity @Table( name = "net_user_role")
public class UserRole {
    
    @Id
    @NotNull @NotEmpty
    private  String id_user;
    
    @NotNull @NotEmpty
    private  String id_role;
    
}

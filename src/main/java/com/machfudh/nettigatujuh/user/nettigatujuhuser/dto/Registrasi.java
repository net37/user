/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.user.nettigatujuhuser.dto;

import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Moh Machfudh
 */
@Data
public class Registrasi {
    
    @NotNull @NotEmpty
    private String username;
    
    @NotNull @NotEmpty
    private String password;
    
    @NotNull @NotEmpty
    private String noanggota;
    
}
